import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

def samples_mutload(samples):
    return samples.groupby(["Tumor_Sample_Barcode"])["Hugo_Symbol"].count().reset_index()

def cancers_mutload(samples):
    return samples.groupby(["Cancer_Type", "Tumor_Sample_Barcode"])["Hugo_Symbol"].count().reset_index().groupby(["Cancer_Type"]).mean().reset_index()

def classify_to_snps_and_indels(data):
    indels = data[data["Variant_Type"].isin(["INS","DEL"])]
    snps = data[data["Variant_Type"].isin(["SNP"])]
    return snps,indels

def get_genes_length(data, GeneNames, GeneLength) : 
    names = set(data['Hugo_Symbol'].to_list()).intersection(GeneNames)
    return GeneLength[GeneLength['Hugo_Symbol'].isin(list(names)) == True]['totalLength'].astype(np.float64).sum()

def plot_caners_mutrate(cancers):
    width = 0.4
    plt.rcParams["figure.figsize"] = [20,20]
    plt.rcParams["axes.labelsize"] = 20

    plt.bar(cancers['Cancer_Type'], cancers['Hugo_Symbol_x'], width, label='MRM')
    plt.bar([i+width for i in np.arange(cancers['Cancer_Type'].shape[0])],
            cancers['Hugo_Symbol_y'], width, label='NON MRM')

    plt.xticks(cancers['Cancer_Type'], rotation=90)

    plt.xlabel("Cancer Type")
    plt.ylabel("Number of Mutations")
    plt.legend()
    # set parameters for tick labels
    plt.tick_params(axis='x', which='major', labelsize=10)
    plt.title("Number of Mutations in MRM and non MRM Cancers")
    plt.savefig("output.png")

if __name__ == "__main__":

    fields = ['Tumor_Sample_Barcode', 'Hugo_Symbol', 'Variant_Classification', 'Variant_Type', "Cancer_Type"]
    mrms = pd.read_csv("data/mrms.maf.gz", sep="\t", usecols=fields, header=0)
    nonmrms = pd.read_csv("data/nonmrms.maf.gz", sep="\t", usecols=fields, header=0)
        
    # GeneLength = pd.read_csv("data/genes_lengths_2009.csv",header=0)

    # GeneLength = GeneLength[['Hugo_Symbol','totalLength']]
    # GeneNames = set(GeneLength['Hugo_Symbol'].to_list())

    # get only mutations in genes with existing length
    # mrms = mrms[mrms['Hugo_Symbol'].isin(GeneNames)]
    # nonmrms = nonmrms[nonmrms['Hugo_Symbol'].isin(GeneNames)]
    mrms_mutload = samples_mutload(mrms)
    nonmrms_mutload = samples_mutload(nonmrms)
    
    #  / get_genes_length(mrms, GeneNames, GeneLength)
    # nonmrm_tumors_avg_muts = nonmrms.groupby(["Tumor_Sample_Barcode"])["Tumor_Sample_Barcode"].count()
    #  / get_genes_length(nonmrms, GeneNames, GeneLength)
    # print(mrm_tumors_avg_muts)
    print(f"Tumors with MRMs: {mrms['Tumor_Sample_Barcode'].unique().shape[0]}, Average Mutation Load: {mrms_mutload['Hugo_Symbol'].mean()}")
    print(f"Tumors without MRMs: {nonmrms['Tumor_Sample_Barcode'].unique().shape[0]}, Average Mutation Load: {nonmrms_mutload['Hugo_Symbol'].mean()}")
    print()
    
    # mrms_snps,mrms_indels = classify_to_snps_and_indels(mrms)
    # nonmrms_snps,nonmrms_indels = classify_to_snps_and_indels(nonmrms)

    # print(f"MRM Tumors with snps: {mrms_snps['Tumor_Sample_Barcode'].unique().shape[0]}, Average Mutation Rate: {mrms_snps.groupby(['Tumor_Sample_Barcode'])['Tumor_Sample_Barcode'].count().mean() / get_genes_length(mrms_snps, GeneNames, GeneLength)}")
    # print(f"MRM Tumors with indels: {mrms_indels['Tumor_Sample_Barcode'].unique().shape[0]}, Average Mutation Rate: {nonmrms_indels.groupby(['Tumor_Sample_Barcode'])['Tumor_Sample_Barcode'].count().mean() / get_genes_length(mrms_indels, GeneNames, GeneLength)}")
    # print()
    # print(f"Non-MRM Tumors with snps: {nonmrms_snps['Tumor_Sample_Barcode'].unique().shape[0]}, Average Mutation Rate: {nonmrms_snps.groupby(['Tumor_Sample_Barcode'])['Tumor_Sample_Barcode'].count().mean() / get_genes_length(nonmrms_snps, GeneNames, GeneLength)}")
    # print(f"Non-MRM Tumors with indels: {nonmrms_indels['Tumor_Sample_Barcode'].unique().shape[0]}, Average Mutation Rate: {nonmrms_indels.groupby(['Tumor_Sample_Barcode'])['Tumor_Sample_Barcode'].count().mean() / get_genes_length(nonmrms_indels, GeneNames, GeneLength)}")

    # merged = pd.merge(group_by_cancer_type(mrms), group_by_cancer_type(nonmrms), on="Sites & Histologies").reset_index().rename(columns={"Hugo_Symbol_x": "MRM_Mutations", "Hugo_Symbol_y": "Non_MRM_Mutations"})
    mrms_cancers_mutload = cancers_mutload(mrms)
    nonmrms_cancers_mutload = cancers_mutload(nonmrms)

    plot_caners_mutrate(pd.merge(mrms_cancers_mutload, nonmrms_cancers_mutload, on=["Cancer_Type"]))