import pandas as pd


if __name__ == "__main__":
    print("Reading Tumors...")
    tumors = pd.read_csv("data/tumors.maf.gz", sep="\t", header=0)
    print("Reading Cancers...")
    cancers = pd.read_csv("data/cancers.csv", header=0)
    tumors['Patient_Barcode'] = tumors['Tumor_Sample_Barcode'].apply(lambda x: x[0:12])
    print("Adding Cancer Type column...")
    res = pd.merge(tumors, cancers, how="left", on="Patient_Barcode")
    print("Saving Tumors...")
    res.to_csv("tumors.maf.gz",sep="\t",index=False)