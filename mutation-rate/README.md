# Mutation Rate

# Setup

1. From the command line create a virtual environment and activate.
```sh
> python3 -m venv .venv
> source .venv/bin/activate
```
2. Install dependencies.
```sh
> pip install -r requirements.txt
```
3. Run.
```sh
> python main.py
```