from numpy import delete
import pandas as pd

if __name__ == "__main__":
    print("Reading Tumors...")
    tumors = pd.read_csv("tumors.maf.gz", sep="\t", header=0)

    print("Reading Repair Genes...")
    repair_genes = pd.read_csv("data/repair_genes_pathways.csv", header=0)
    
    print("Splitting Tumors...")
    tumors_with_mrms = []
    tumors_without_mrms = []
    grouped_tumors = tumors.groupby(["Tumor_Sample_Barcode"])
    for _, values in grouped_tumors:
        if (values["Hugo_Symbol"].isin(repair_genes["Hugo_Symbol"].values) & values["Variant_Classification"].isin(['Nonsense_Mutation','Missense_Mutation'])).any():
            [tumors_with_mrms.append(v) for v in values.to_numpy()]
        else:
            [tumors_without_mrms.append(v) for v in values.to_numpy()]
    
    cols = tumors.columns
    del tumors # free memory to prevent crashing
    mrms, nonmrms = pd.DataFrame(tumors_with_mrms, columns=cols), pd.DataFrame(tumors_without_mrms, columns=cols)
    print("Saving MRMs...")
    mrms.to_csv("mrms.maf.gz",sep="\t",index=False)
    print("Saving non MRMs...")
    nonmrms.to_csv("nonmrms.maf.gz",sep="\t",index=False)
    