import pandas as pd
import numpy as np

def calc_genes_lengths(inputfile, outputfile):
    """Calculate genes lengths using and write them into a csv file

    Args:
        inputfile (string): input maf filename
        outputfile (string): output csv filename.
    """    
    Genes = pd.read_csv(inputfile,sep="\t",header=0)
    
    exonStarts = Genes["exonStarts"].to_numpy()
    exonEnds = Genes["exonEnds"].to_numpy()
    cdsEnd = Genes["cdsEnd"].to_numpy().astype("int64")
    cdsStart = Genes["cdsStart"].to_numpy().astype("int64")
    utr5 =  []
    utr3 =  []
    utr = 0
    exonlengths = []

    for i in range(len(exonStarts)) :
        endArr = np.array(exonEnds[i][:-1].split(",")).astype("int64")
        startArr = np.array(exonStarts[i][:-1].split(",")).astype("int64")
        lengths = endArr - startArr
        sumL = sum(lengths)
        exonlengths.append(sumL)

        for start in startArr[::-1]:
            if cdsStart[i] >= start:
                utr = cdsStart[i] - start
                break

        if list(startArr).index(start) != 0:
            utr = utr + sum(endArr[0:list(startArr).index(start)] - startArr[0:list(startArr).index(start)])
        utr5.append(utr) 

        for end in endArr:
            if cdsEnd[i] <= end:
                utr = end - cdsEnd[i]
                break

        if list(endArr).index(end) != len(endArr) - 1:
            utr = utr + sum(endArr[list(endArr).index(end) + 1:] - startArr[list(endArr).index(end) + 1 :])
        utr3.append(utr)

    utr_arr = np.array(utr5) + np.array(utr3)

    Genes["UrtLength"] = utr_arr
    Genes["exonlengths"] = exonlengths
    Genes["totalLength"] = Genes["exonlengths"] - Genes["UrtLength"]
    Genes.rename(columns={"geneSymbol": "Hugo_Symbol"},inplace=True)
    Genes[Genes["totalLength"] != 0].to_csv(outputfile, index=False)

if __name__ == "__main__":
    calc_genes_lengths("data/genes2009.maf.gz", "data/genes_lengths_2009.csv")