# Clonal Signatures

# Setup

requires python==2.7

1. From the command line create a virtual environment and activate.
```sh
> virtualenv -p /usr/bin/python2.7 .venv
> source .venv/bin/activate
```
2. Install dependencies.
```sh
> pip install -r requirements.txt
```
3. Run.
```sh
> python main.py
```
