# Contributing

All changes should take place in a separate branch submitted to the master in the form of a _pull request_.

## Steps:

1. Create a new Branch.

```sh
# substitute branchName with whatever suits you
> git checkout -b branchName
```

2. After commiting your changes, pull from origin and rebase to make sure you have the latest changes from the master.

```sh
> git checkout master
> git pull origin master
> git checkout branchName
> git rebase master
```

3. Push your branch to the origin. Go to the repo then create the _pull request_.

```sh
> git push origin branchName
```

4. After your changes had been successfully merged into the master branch, your branch will be deleted at the origin but not locally, so make sure you clean up your local.

```sh
> git branch -D branchName
```

Useful commit messages that describe what you have been working on are greatly appreciated, here is a really great [guide](https://www.conventionalcommits.org/en/v1.0.0/#summary) for writting a helpful commit message.