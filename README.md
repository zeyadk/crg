# CRG

The project is structured into separate directories. The *README* in each directory describes how to setup and run the directory. Furthermore, each directory requires a set of data files to function correctly, these files can be downloaded from [here](https://1drv.ms/u/s!Ah_oHERboezWkAwGnZQC7HxwyLar?e=dUrMii)