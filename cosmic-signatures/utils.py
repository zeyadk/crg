import pandas as pd

def get_context(x):
    match_dict = {'A': 'T', 'C': 'G', 'G': 'C', 'T': 'A'}
    if x['Reference_Allele'] in ('C', 'T'):
        context = x['CONTEXT'][4] + '[' + x['Reference_Allele'] + '>' + x['Allele'] + ']' + \
            x['CONTEXT'][6]
    else:
        context = match_dict[x['CONTEXT'][6]] + '[' + match_dict[x['Reference_Allele']] +\
            '>' + match_dict[x['Allele']] + ']' + match_dict[x['CONTEXT'][4]]
    return context

def get_tumor_mutations(itr):
    rows = next(itr)
    cur = rows["Tumor_Sample_Barcode"].values[0]
    for row in itr:
        if cur == row["Tumor_Sample_Barcode"].values[0]:
            cur = row["Tumor_Sample_Barcode"].values[0]
            rows = rows.append(row)
        else:
            prev = cur
            cur = row["Tumor_Sample_Barcode"].values[0]
            cpy = rows.copy()
            rows = pd.DataFrame([],columns=row.columns)
            rows = rows.append(row)
            yield prev,cpy
    yield cur,rows

itr = pd.read_csv("data/mrms.maf",sep="\t",chunksize=1, header=0)
s,t = next(get_tumor_mutations(itr))
rg = pd.read_csv("data/repair_genes_pathways.csv",header=0)["Hugo_Symbol"]
r = t[t["Hugo_Symbol"].isin(rg.values)][["Hugo_Symbol","Variant_Classification","Allele","CONTEXT"]]
r.to_csv("test.csv")