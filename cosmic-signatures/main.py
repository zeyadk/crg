import pandas as pd 

nonmrms_path = "data/nonmrms_signature.csv"
mrms_path = "data/mrms_signature.csv"

mrms_signatures = pd.read_csv(mrms_path,index_col=0)
nonMrms_signatures = pd.read_csv(nonmrms_path,index_col=0)
print(mrms_signatures.mean() - nonMrms_signatures.mean())